#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <cstring>
#include <stdlib.h>
#include <math.h>
#include <fstream>
#include "Kernel.cu"
#include "Cuda_Information.cu"


// Define la distribucion de threads en la 3 cordenadas
#define BLOCK_X 16
#define BLOCK_Y 16
#define BLOCK_Z 4

#define BLOCK_Media 512

using namespace std;

#define HANDLE_ERROR( err ) ( HandleError( err, __FILE__, __LINE__ ) )

class Cuda_Functions : public Cuda_Information
{
	public:
		
		Cuda_Functions(int Width, int Heigth, int Depth)
		{
			HANDLE_ERROR (cudaMalloc((void **)&DataArrayGPU , Width*Heigth*Depth*sizeof(float) ) );
			HANDLE_ERROR (cudaMalloc((void **)&DataArrayPrevGPU , Width*Heigth*Depth*sizeof(float) ) );
		    DataArray=(float *) malloc (Width*Heigth*Depth*sizeof(float)); 
			DataArrayAux=(float *) malloc (Width*Heigth*Depth*sizeof(float)); 
		    DataArrayPrev=(float *) malloc (Width*Heigth*Depth*sizeof(float)); 
			devices=0;
			Media=0;
		}
		~Cuda_Functions()
		{
			  free(DataArray);	
			  free(DataArrayPrev);
			  cudaFree (DataArrayGPU);
			  cudaFree (DataArrayPrevGPU); 
		}  

		float Calculo_Media(int Width, int Heigth, int Depth)
		{
			// Este proceso hace una reducci�on en CUDA, la idea es utilizar una tecnica de memoria compartida 
			// Aprovechamos que los datos en este punto estan alineado en un vector para trabajar la GPU.
			// Por lo tanto esta dise�ado en una sola di+mension. En este punto no hacemos copia de memoria porque ya los datos estan
			// en la memoria de la GPU.
		
			long  TotalThreads=(Width*Heigth*Depth);
			
			HANDLE_ERROR (cudaMalloc((void **)&SumOutputGPU ,  Width*Heigth*Depth*sizeof(float) ) );

			SumOutput=(float *) malloc (Width*Heigth*Depth*sizeof(float)); 

			dim3  DimGrid((TotalThreads + BLOCK_Media - 1)/BLOCK_Media ,1,1); // Estoy Asignando bloques de 512 Threads el m�aximo que soporta la tarjeta es 1024. 
			dim3  DimBlock(BLOCK_Media,1,1);
			
			MeanReduction<<<DimGrid, DimBlock  >>> (DataArrayGPU ,  SumOutputGPU,  TotalThreads);
			HANDLE_ERROR(cudaMemcpy(SumOutput ,  SumOutputGPU,  Width*Heigth*Depth * sizeof(float), cudaMemcpyDeviceToHost));
			
			
		    // Una vez calculado la suma en el kernel, se obtiene como resultado un solo valor por cada bloque definido
			// al final se debe empaquetar todos los resultados en la CPU.  

			Media=0;
			for (int i=0; i<= (TotalThreads + BLOCK_Media - 1)/BLOCK_Media; i++)
				Media+=*(SumOutput+i);
			return(Media);
		}

		void run(float* Aux, int Width, int Heigth, int Depth, int numberofDevice)
		{ 
			/* Este proceso hace la operaci�n de c�lculo del procesado de la imagen en CUDA para el bloque definido del fichero	*/

			// RAUL: memleak sobre DataArray?
			DataArray=Aux; // Transfiriendo Array con los elementos seleccionados para computar en la GPU
			memset(DataArrayPrev, 0, Width*Heigth*Depth*sizeof(float));
			
			// Copia la informacion parcial de la CPU -> GPU
			HANDLE_ERROR (cudaMemcpy (DataArrayGPU, DataArray, Width*Heigth*Depth *sizeof(float) , cudaMemcpyHostToDevice));
			HANDLE_ERROR (cudaMemcpy (DataArrayPrevGPU , DataArrayPrev, Width*Heigth*Depth*sizeof(float) , cudaMemcpyHostToDevice));

			// En este punto se debe calcular dependiendo de Width, Height and Depth, Etc. W Tenemos que recordar las limitaciones de 1024 thread por bloque.

			dim3  DimGrid(BLOCK_X,BLOCK_Y,BLOCK_Z); // Estoy Asignando bloques de 512 Threads el m�aximo que soporta la tarjeta es 1024. 
			dim3  DimBlock(ceil(float((Width+BLOCK_X-1)/BLOCK_X)),ceil(float((Heigth+BLOCK_Y-1)/BLOCK_Y)), ceil(float((Depth+BLOCK_Z-1)/BLOCK_Z)));

			// Llamado del Kernel

			ImageProcessing<<< DimGrid, DimBlock >>> (DataArrayGPU , DataArrayPrevGPU, Width, Heigth, Depth);

			//Copia los datos de regreso de la GPU a la CPU
			HANDLE_ERROR (cudaMemcpy( DataArrayAux , DataArrayGPU, Width*Heigth*Depth*sizeof(float) , cudaMemcpyDeviceToHost));

			

		}

		void SaveIntoFile(int Width, int Heigth, int Depth)
		{
			// Aqu� se copia en el archivo final el mismo esta definido para copiar los nuevos datos al final del archivo
			ofstream Final_Results("Output_File_images.bin",ios::out | ios::app  | ios::binary);
			Final_Results.seekp(0, Final_Results.end);
			for (int z=0; z<Depth; z++)
			{
					for (int y=0; y<Heigth; y++)
					{
						for (int x=0; x<Width; x++)
							Final_Results <<*(DataArrayAux+(Width*Heigth*z) + (Width*y) + x)<<" ";
						Final_Results <<endl;
					}
					Final_Results <<endl;
			}
			Final_Results.close();
		}

		void GeneradorEntrada(int Width, int Heigth, int Depth)
		{
			ofstream Myfile_Output;
			Myfile_Output.open ("Input_File_images.bin",  ios::binary );
			float p=1.1;
			for(int z=0; z<Depth; z++) {
				for(int y=0; y<Heigth; y++){
					for(int x=0; x<Width; x++){
						Myfile_Output <<p<<" ";
					}
					Myfile_Output <<endl;
				}
				Myfile_Output <<endl;
				p = float(p + 1.0);
			}
			Myfile_Output.close();
		}

private:
	float *dev_a;
	int devices;
	int WarpsSize;
	float *DataArray;
	float *DataArrayAux;
	float *DataArrayGPU;
	float *DataArrayPrev;
	float *DataArrayPrevGPU;
	float *SumOutput;
	float *SumOutputGPU;
	float Media;
	
};


/* Desarrollada por Ronal Muresano 2016

En esta clase define 2 elementos para la lectura de la informaci�n de la
tarjeta GPU.   La primera funci�n es para la gesti�n de errores de CUDA.
La segunda funci�n CountDevice calcula el n�mero de dispositivos GPU y finalmente 
la tercera funci�n Memory Information, determina el tama�o de la memoria de cada GPU. 
Esta informaci�n ser� utilizada para calcular el tama�o ideal del bloque a calcular.

*/

#include <iostream>
#include <stdio.h>
#include <cuda.h>
#include "cuda_runtime.h"

#define DEBUG 0
#define HANDLE_ERROR( err ) ( HandleError( err, __FILE__, __LINE__ ) )
using namespace std;

class Cuda_Information
{

public:
	Cuda_Information()
	{
		devices=0;
	}
	~Cuda_Information()
	{
		devices=0;
	}
		static void HandleError( cudaError_t err, const char *file, int line )
		{
			if (err != cudaSuccess)
			{
				cout<<cudaGetErrorString( err )<<" in "<< file<< " at line "<<line<<endl;
				exit( EXIT_FAILURE );
			}
		}


	int CountDevice()
		{
			HANDLE_ERROR(cudaGetDeviceCount(&devices));
			return(devices);
		}

		int* Gpu_Memory_Information()
		{
			int *MemorySize=(int *) malloc (devices*sizeof(int)); 

			for (int i = 0; i < devices; i++) 
			{
				cudaDeviceProp prop;
				HANDLE_ERROR(cudaGetDeviceProperties(&prop, i));
				*(MemorySize+i)= prop.totalGlobalMem;
#ifdef DEBUG 
				cout<<" Device Number: "<<i<<endl;
				cout<<" Device name:    "<<prop.name<<endl;
				cout<<" Global Memory Available in B:    "<<prop.totalGlobalMem<<endl;
				cout<<" Warps Size:    "<<prop.warpSize<<endl;
				cout<<" Max Thread per Block:   "<<prop.maxThreadsPerBlock<<endl;
				cout<<" Max Threads Dim :    X: "<<prop.maxThreadsDim[0] << "  Y : "<<prop.maxThreadsDim[1]<< "  Z : "<<prop.maxThreadsDim[2]  <<endl;
				cout<<" Max Grid Size   :    X: "<<prop.maxGridSize[0]   << "  Y : "<<prop.maxGridSize[1]  << "  Z : "<<prop.maxGridSize[2]  <<endl;
#endif
			}
			return(MemorySize);
	   }

private:
	int devices;

};
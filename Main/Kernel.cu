#include <cuda.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "device_functions.h"

// Se debe configurar para que las dimensiones puedan ser diferentes Height depth and width
__global__ void ImageProcessing( float *DataArrayGPU , float *DataArrayPrevGPU , int  Width,  int Heigth, int Depth)
{
	  //Se definen la dimensiones de acuerdo con los bloques y threads definidos en las variables DimBlock GridBlock

	  int Xindex= blockDim.x * blockIdx.x  +  threadIdx.x;
	  int Yindex= blockDim.y * blockIdx.y  +  threadIdx.y;
	  int Zindex= blockDim.z * blockIdx.z  +  threadIdx.z;

	  float DatoUnidimenEnXYZ;
      // Se controlan todos los elementos que esten dentro de las fronteras

	  if( ( Xindex < Width) && ( Yindex < Heigth ) && ( Zindex < Depth ) ) 
	  {     // Se calcula afuera para evitar la operaci�n dentro del bucle vrias veces, al final se convierte en un valor constante.
			
		    float Component= (float(Xindex) / (1 + (float) Yindex)); 
			
			int index=(( Heigth* Width*Zindex) + (Width*Yindex) + Xindex);

			// Los Vectores est�n alineados en memoria y son controlados por el c�lculo de index. 

			for (int i=0;i<5; i++)
			{
				DatoUnidimenEnXYZ=*(DataArrayGPU + index);
				*(DataArrayGPU + index)=(*(DataArrayPrevGPU  +  index)  + *(DataArrayGPU + index))/ 3.0;
				*(DataArrayPrevGPU  +  index) = DatoUnidimenEnXYZ *Component; 
			}
	  }
}

__global__ void MeanReduction(float *DataArrayGPU, float *SumOutputGPU,  int Size)
{
	
	__shared__  float SumaParcial[512];
	unsigned int t= threadIdx.x;

	int i = blockDim.x*blockIdx.x + threadIdx.x;
	if (i<Size)
	{
		SumaParcial[t]=*(DataArrayGPU + i );
		for (int stride=blockDim.x/2; stride > 0; stride>>=1)
		{
			__syncthreads();
			if (t < stride)
				SumaParcial[t]+=SumaParcial[t + stride];

			__syncthreads();
		}
		if (t==0)
			*(SumOutputGPU + blockIdx.x) =SumaParcial[0];
	}
}

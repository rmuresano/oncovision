#include <iostream>
#include <stdlib.h>
#include <cuda.h>
#include <fstream>
#include <ctime>
#include <windows.h>
#include <string>

#include "cuda_runtime.h"
#include "CudaClass.cu"

#define Total_Image_Width  80
#define Total_Image_Heigth 80
#define Total_Image_Depth  40

#define Flag_Input_Generation 1

#define Flag_File_Division 0



using namespace std;


int main()
{
	double cpuTime;
	clock_t start, end1; // Definici�n de Timers.
	
	int nDevices;
	
	// Este objeto es creado solamente para calcular el tam�o ideal necesario de cada memoria GPU, se usa solamente para determinar el tama�o de la memoria de la GPU.
	Cuda_Information Memory; 

	// Se calcula el n�mero de dispositivos para utilizarlos en la programaci�n multiGPU

	/*System Information using CUDA */

	// Se determina el n�mero de dispositivos

	nDevices=Memory.CountDevice();

	// Este vector contiene el tama�o de la memoria de cada dispositivo.
	

	int *MemorySize=(int *) malloc (nDevices*sizeof(int));  
	
	MemorySize=Memory.Gpu_Memory_Information();

	// C�lculo el tama�o de la memoria necesaria para guardar el archivo. A partir de aqu� se calcula el valor ideal de Z que es la coordenada que se dividir�

	unsigned long int MemoryRequired= (Total_Image_Width * Total_Image_Heigth * Total_Image_Depth * 4);  

	// Aqu� se calcula el valor de Z para cada GPU dependiendo del n�mero de GPUs encontradas. 
	cout<<endl;
	cout<<"Memoria de la GPU "<< *MemorySize<<endl;
	cout<<"Memoria Necesaria para la ejecuci�n  "<< MemoryRequired<<endl;

	// En este punto el programa sabe el tama�o de la memoria de la GPU (MemorySize: es un puntero que tiene el tama�o de memoria de cada dispositivo)
	// y el n�mero de dispositivos (nDevices).
	int *IdealZ= (int *) malloc (nDevices*sizeof(int));
	if (Flag_File_Division)
	{
		for (int i=0; i<nDevices; i++)
		{	
			*(IdealZ+i)=Total_Image_Depth; //Comienza con el tama�o mas grande y lo ir� dividiendo a medida que el programa no soporte la ejecuci�n
			while (*(MemorySize+i)<MemoryRequired)
			{
				*(IdealZ+i) = int (Total_Image_Depth/8 ); // dividimos el tama�o en 8 debido a que debemos reservar espacio para dos vectores DatarArray, DataArrayAux y luego SumOutputGPU para el reduction.
				MemoryRequired=MemoryRequired/8;
			}
		}
	}
	else
	{
		*(IdealZ)= 4; // He colocado este valor solamente como un problema ejemplo en caso de que se quiera ejecutar un problema m�s peque�o y ver la divisi�n de los archivos. 
	}

	
	Cuda_Functions Value(Total_Image_Width, Total_Image_Heigth, *(IdealZ));

	float Media=0;
	
	if(Flag_Input_Generation==1)
		// Aqui se llama a la funcion para crear un fichero de prueba 
		Value.GeneradorEntrada(Total_Image_Width, Total_Image_Heigth,Total_Image_Depth);
	
	start = clock();

	ifstream Myfile_Read;
	Myfile_Read.open("Input_File_images.bin",ios::binary | ios::in);
	if (Myfile_Read.fail())
	{
		 cout<<"\n FILE DOES NOT EXIST \n";
		 exit( EXIT_FAILURE );
	}
	else
	{
			/*  Leo el fichero en Memoria por pedazos considerando solamente dos dimensiones X Y y la tercera Z la divido en partes por ejemplo si
		la memoria tiene un tama�o de 1 GB entonces dividir� 2000x2000xIdealZ donde IdealZ ser� cuantos valores completan el tama�o de la memoria. Toda esta
		informaci�n ser� alineada en memoria*/ 
		float *Sub_Matrix=(float *) malloc (Total_Image_Width*Total_Image_Heigth*(*(IdealZ))*sizeof(float)); 
		char cNum[10] ;
		int Processing  = Total_Image_Width * Total_Image_Heigth * (*(IdealZ));
	
		cout<<endl;
		cout<<"************************************"<<endl;
		cout<< " El archivo se ha divido en "<<ceil(float(Total_Image_Depth/(*(IdealZ))))<<" Bloques"<<endl<<endl;
	
		int Bloq=1;
		while(!Myfile_Read.eof())
			{	
				int i=0;
				cout<< "Ejecutando Bloque  "<<Bloq<<"/"<<ceil(float(Total_Image_Depth/(*(IdealZ))))<<" Ejecutado"<< endl<<endl;
				while ((Myfile_Read.getline(cNum, 256, ' ')) && (i<Processing))
				{
					*(Sub_Matrix+i)= atof(cNum);
					i++;
				}
				// Aqui se hace el proceso de calculo en la GPU. 
				Value.run(Sub_Matrix, Total_Image_Width, Total_Image_Heigth, *(IdealZ), nDevices);	

				// una vez procesado el bloque hacemos el llamado a 
				Media = Media + Value.Calculo_Media(Total_Image_Width, Total_Image_Heigth, *(IdealZ));

				Value.SaveIntoFile(Total_Image_Width, Total_Image_Heigth, *(IdealZ));
				Bloq++;
			}
			Myfile_Read.close();
	}
	

	Media=(Media/(Total_Image_Width * Total_Image_Heigth * Total_Image_Depth));
	cout<<endl;
	cout << "La Media de todos los voxels es "<< Media<<endl;
	cout<<"************************************"<<endl;
	cout << "Ejecuci�on Finalizada "<< endl<< endl;
	
	end1 = clock();
	cpuTime= float (end1 - start) / (CLOCKS_PER_SEC);
	cout<<"Execution time :  "<< cpuTime<<"  Images ["<<Total_Image_Width<<"x"<<Total_Image_Heigth<<"x"<<Total_Image_Depth<<"]"<< endl;
	system("pause");
	return 0 ;
}
